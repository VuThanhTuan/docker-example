const express = require("express");
const cors = require("cors");
const app = express();

const port = 8080;

app.use(cors());
app.get("/", function (req, res) {
  res.send({
    data: ["ASP.NET", "Java", "Nodejs", "Angular"],
  });
});

app.listen(port);
console.log(`Running on port ${port}`);
